<?php

class DB {
	private $server = 'localhost';
	private $user = 'pushko';
	private $password = '';
	private $dbname = 'wunder';
	public $conn;

	public function __construct() {
		$this->connect();
	}

	private function connect() {
		$this->conn = new mysqli($this->server, $this->user, $this->password, $this->dbname);
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
		return true;
	}

	public function close() {
		return $this->conn->close();
	}

	public function query($query) {
		return $this->conn->query($query);
	}

	public function get_last_inserted_id() {
		return $this->conn->insert_id;
	}
}