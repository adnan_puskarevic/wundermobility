<?php

class Curl {
	private $cust_id;
	private $iban;
	private $owner;
	private $parameters;
	private $curl_request;
	private $result;
	private $url = "https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data";

	public function __construct() {
	}

	public function execute_post_requesst() {
		$this->init();
		$this->prepare_parameters();
		$this->prepare_post_request();

		$this->result = $this->exec();
		return $this->handle_response();
	}

	public function set_parameters($cust_id, $iban, $owner) {
		$this->cust_id = $cust_id;
		$this->iban = $iban;
		$this->owner = $owner;
	}

	private function init() {
		$this->curl_request = curl_init();
	}

	private function prepare_parameters() {
		$parameters = array(
			"customerId" => $this->cust_id,
			"iban" => $this->iban,
			"owner" => $this->owner
		);

		$this->parameters = json_encode($parameters);
	}

	private function prepare_post_request() {
		curl_setopt($this->curl_request, CURLOPT_URL, $this->url);
		curl_setopt($this->curl_request, CURLOPT_POST, 1);
		curl_setopt($this->curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
		curl_setopt($this->curl_request, CURLOPT_HEADER, 0);
		curl_setopt($this->curl_request, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($this->curl_request, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($this->curl_request, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($this->curl_request, CURLOPT_FOLLOWLOCATION, 0);
		curl_setopt($this->curl_request, CURLOPT_HTTPHEADER,  array(
			"Content-Type: application/json",
		));
		curl_setopt($this->curl_request, CURLOPT_POSTFIELDS, $this->parameters);
	}

	private function exec() {
		return curl_exec($this->curl_request);
	}

	private function get_info() {
		return curl_getinfo($this->curl_request, CURLINFO_HTTP_CODE);
	}

	private function close() {
		return curl_close($this->curl_request);
	}

	private function handle_response() {
		$status = $this->get_info();
		if (intval($status) != 200) {
			$this->handle_error($status);
		}

		$this->close();
		return $this->handle_success();
	}

	private function handle_error($status) {
		$result_json = json_decode($this->result);
		if (!empty($result_json->error)) {
			error_log($result_json->error. ", status: {$status}");
		}
		
		$this->close();

		return null;
	}

	private function handle_success() {
		$return_data = json_decode($this->result);
		return @$return_data->paymentDataId;
	}
}