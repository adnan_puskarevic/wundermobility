<?php

class Messages {
	public function __construct() {

	}

	public static function get_error_message() {
		return <<< HTML
		<div>
			<p>An error occurred. Customer could not be created!</p>
		</div>
HTML;
	}

	public static function get_successful_message($paymentDataId) {
		return <<< HTML
		<div>
			<p>Customer successfully created!</p>
			<p>paymentDataId: $paymentDataId</p>
		</div>
HTML;
	}
}