<?php

function autoload () {
	require_once 'messages.php';
	require_once 'model.php';
	require_once 'db.php';
	require_once 'curl.php';
	require_once 'controller.php';
	require_once 'tests/test.php';
}

spl_autoload_register("autoload");

if(!isset($_SESSION)) {
	session_start();
}

if (empty($_SESSION['step'])) {
	$_SESSION['step'] = 1;
}

$uri = parse_url($_SERVER['REQUEST_URI']);
if (@$uri['query'] === "ajax") {
	$customer = new Customer();
	$db = new DB();
	$curl = new Curl();

	$controller = new Controller($customer, $db, $curl);

	if (!empty($_SESSION['customer_data'])) {
		$controller->get_session_data();
	}

	$controller->handle_ajax();
} else if (@$uri['query'] === "test=pass") {
	Test::get_test_pass();
} else if (@$uri['query'] === "test=failed") {
	Test::get_test_failed();
} else if ('/index.php' === $uri['path']) {
	require "form_view.php";
} else {
	header('HTTP/1.1 404 Not Found');
	echo '<html><body><h1>Page Not Found</h1></body></html>';
	exit;
}