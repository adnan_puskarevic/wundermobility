<?php

class Customer {
	public $id;
	public $first_name;
	public $last_name;
	public $telephone;
	public $street;
	public $house_number;
	public $zip_code;
	public $city;
	public $acc_owner;
	public $iban;
	public $paymentDataId;

	public function __construct() {

	}
}