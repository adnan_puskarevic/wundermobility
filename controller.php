<?php

class Controller {
	private $customer;
	private $db;
	private $curl;

	public function __construct($customer, $db, $curl) {
		if(!isset($_SESSION)) {
			session_start();
		}
		$this->customer = $customer;
		$this->db = $db;
		$this->curl = $curl;
	}

	public function get_session_data() {
		$this->customer = $_SESSION['customer_data'];
	}

	public function getCustomer() {
		return $this->customer;
	}

	public function handle_ajax() {
		if (!empty($_POST['reset_session_data']) && $_POST['reset_session_data'] == 'true') {
			session_unset();
		}

		switch (@$_SESSION['step']) {
			case 1:
				$this->handle_first_step();
			break;

			case 2:
				$this->handle_second_step();
			break;

			case 3:
				$this->handle_third_step();
			break;

			case 4:
				$this->handle_fourth_step();
			break;

			default:
				$_SESSION['step'] = 1;
			break;
		}

		$this->store_session_data();

		echo $_SESSION['step'];
		exit;
	}

	private function handle_first_step() {
		$this->customer->first_name = @$_POST['first_name'];
		$this->customer->last_name = @$_POST['last_name'];
		$this->customer->telephone = @$_POST['telephone'];
		$_SESSION['step'] = 2;
	}

	private function handle_second_step() {
		$this->customer->street = @$_POST['street'];
		$this->customer->house_number = @$_POST['house_number'];
		$this->customer->zip_code = @$_POST['zip_code'];
		$this->customer->city = @$_POST['city'];
		$_SESSION['step'] = 3;
	}

	private function handle_third_step() {
		$this->customer->acc_owner = @$_POST['acc_owner'];
		$this->customer->iban = @$_POST['iban'];
		$_SESSION['step'] = 4;

		$this->submit_form();
		echo Messages::get_successful_message($this->customer->paymentDataId);
		exit;
	}

	private function handle_fourth_step() {
		$_SESSION['step'] = 1;
	}

	private function store_session_data() {
		$_SESSION['customer_data'] = $this->customer;
	}

	private function submit_form() {
		$this->store_customer_in_database();
		if (empty($this->customer->id)) {
			echo Messages::get_error_message();
			exit;
		}

		$this->get_paymentDataId();
		if (!empty($this->customer->paymentDataId)) {
			$this->store_paymentDataId_in_database();
		} else {
			$this->handle_error();
		}
	}

	private function store_customer_in_database() {
		$query = "INSERT INTO customers (first_name, last_name, telephone, street, house_number, zip_code, city, acc_owner, iban)
		VALUES ('{$this->customer->first_name}', '{$this->customer->last_name}', '{$this->customer->telephone}', '{$this->customer->street}', '{$this->customer->house_number}', {$this->customer->zip_code}, '{$this->customer->city}', '{$this->customer->acc_owner}', '{$this->customer->iban}')";

		if ($this->db->query($query)) {
			$this->customer->id = $this->db->get_last_inserted_id();
		}
	}

	private function get_paymentDataId() {
		$this->curl->set_parameters($this->customer->id, $this->customer->iban, $this->customer->acc_owner);
		$this->customer->paymentDataId = $this->curl->execute_post_requesst();

		if (empty($result)) {
			return null;
		}

		return $this->customer->paymentDataId;
	}

	private function store_paymentDataId_in_database() {
		$query = "UPDATE customers SET paymentDataId = '{$this->customer->paymentDataId}' WHERE id = '{$this->customer->id}'";
		return $this->db->query($query);
	}

	private function handle_error() {
		$this->delete_customer_from_database();
		echo Messages::get_error_message();
		exit;
	}

	private function delete_customer_from_database() {
		$query = "DELETE FROM customers WHERE id = '{$this->customer->id}'";
		return $this->db->query($query);
	}
}