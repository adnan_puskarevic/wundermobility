<?php
?>
<form id="registration_form" action="" method="post">
	<fieldset id="step1">
		<div>
			<label for="first_name">First Name</label>
			<input type="text" name="first_name" id="first_name" value="">
		</div>
		<div>
			<label for="last_name">Last Name</label>
			<input type="text" name="last_name" id="last_name" value="">
		</div>
		<div>
			<label for="telephone">Telephone</label>
			<input type="text" name="telephone" id="telephone" value="">
		</div>
		<input type="button" name="cancel" value="Cancel" onclick="reset_registration_form();" />
		<input type="button" name="next" value="Next" onclick="submit_first_view()" />
	</fieldset>
	<fieldset id="step2">
		<div>
			<label for="street">Street</label>
			<input type="text" name="street" id="street" value="">
		</div>
		<div>
			<label for="house_number">House Number</label>
			<input type="text" name="house_number" id="house_number" value="">
		</div>
		<div>
			<label for="zip_code">ZIP Code</label>
			<input type="number" name="zip_code" id="zip_code" value="">
		</div>
		<div>
			<label for="city">City</label>
			<input type="text" name="city" id="city" value="">
		</div>
			<input type="button" name="cancel" value="Cancel" onclick="reset_registration_form();" />
			<input type="button" name="next" value="Next" onclick="submit_second_view()" />
		</div>
	</fieldset>
	<fieldset id="step3">
		<div>
			<label for="acc_owner">Account Owner</label>
			<input type="text" name="acc_owner" id="acc_owner" value="">
		</div>
		<div>
			<label for="iban">IBAN</label>
			<input type="text" name="iban" id="iban" value="">
		</div>
			<input type="button" name="cancel" value="Cancel" onclick="reset_registration_form();" />
			<input type="button" name="next" value="Submit" onclick="submit_third_view()" />
		</div>
	</fieldset>
	<fieldset id="step4">
		<input type="button" name="first_view" class="next btn btn-success" value="Home" onclick="reset_registration_form()" />
	</fieldset>
</form>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<script type="text/javascript">
	function show_hide_views(step) {
		if (step == 0 || step == 1) {
			show_first_view();
		} else if (step == 2) {
			show_second_view();
		} else if (step == 3) {
			show_third_view();
		} else if (step == 4 || step.length > 1) {
			show_fourth_view(step);
		}
	}

	function show_first_view() {
		$("#step1").show();
		$("#step2").hide();
		$("#step3").hide();
		$("#step4").hide();
	}

	function show_second_view() {
		$("#step1").hide();
		$("#step2").show();
		$("#step3").hide();
		$("#step4").hide();
	}

	function show_third_view() {
		$("#step1").hide();
		$("#step2").hide();
		$("#step3").show();
		$("#step4").hide();
	}

	function show_fourth_view(step_data) {
		$("#step1").hide();
		$("#step2").hide();
		$("#step3").hide();
		$("#step4").show();
		if (step_data.length > 1) {
			$("#step4").prepend(step_data);
		}
	}

	function reset_registration_form() {
		$('#registration_form').trigger("reset");
		$('#step4').find('div:first').remove();

		data = "reset_session_data=true";
		send_ajax_request(data);
	}

	function submit_first_view() {
		var first_name = $("#first_name").val();
		var last_name = $("#last_name").val();
		var telephone = $("#telephone").val();

		var data = "first_name=" + first_name + "&last_name=" + last_name + "&telephone=" + telephone;

		send_ajax_request(data);
	}

	function submit_second_view() {
		var street = $("#street").val();
		var house_number = $("#house_number").val();
		var zip_code = $("#zip_code").val();
		var city = $("#city").val();

		var data = "street=" + street + "&house_number=" + house_number + "&zip_code=" + zip_code + "&city=" + city;

		send_ajax_request(data);
	}

	function submit_third_view() {
		var acc_owner = $("#acc_owner").val();
		var iban = $("#iban").val();

		var data = "acc_owner=" + acc_owner + "&iban=" + iban;

		send_ajax_request(data);
	}

	function send_ajax_request(data) {
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				eval(show_hide_views(this.responseText));
			}
		};
		xhttp.open("POST", "./index.php?ajax", true);
		xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhttp.send(data+"&ajax=true");
	}

	show_hide_views(<?php echo intval(@$_SESSION['step']) ?>);
</script>